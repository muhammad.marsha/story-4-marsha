from django.urls import path

from . import views

urlpatterns = [
    path('', views.matkul, name='matkul'),
    path('delete/<int:id>', views.delete, name = 'delete'),
    path('detail/<int:id_detail>', views.detail, name = 'detail'),
    path('form/', views.formku, name = 'forms'),
]