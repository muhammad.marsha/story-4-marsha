from django.shortcuts import render, redirect
from marsha.models import KelasModel
from .forms import KelasForm

# Create your views here.

def matkul(request):
    posts = KelasModel.objects.all()
    kelas_form = KelasForm(request.POST or None)
    context = {
        'data_form': kelas_form,
        'posts':posts,
    }
    if request.method =='POST':
        if kelas_form.is_valid:
            kelas_form.save()

    return render(request, 'matkul.html', context)

def formku(request):
    posts = KelasModel.objects.all()
    kelas_form = KelasForm(request.POST or None)
    context = {
        'data_form': kelas_form,
        'posts':posts,
    }
    if request.method =='POST':
        if kelas_form.is_valid:
            kelas_form.save()

    return render(request, 'form.html', context)

def delete(request, id):
    KelasModel.objects.filter(id=id).delete()
    return redirect('')

def detail(request, id_detail):
    id_matkul = KelasModel.objects.get(id=id_detail)
    return render(request, 'detail.html', {'post': id_matkul})