from django import forms
from django.forms import ModelForm
from marsha.models import KelasModel

class KelasForm(forms.ModelForm):
    class Meta:
        model = KelasModel
        fields = [
            'nama',
            'dosen',
            'sks',
            'deskripsi',
            'smt',
            'TA',
            'ruang',
        ]

        widgets = {
            'nama': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Mata Kuliah'
            }
            ),
            'dosen': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Nama Dosen'
            }
            ),
            'deskripsi': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Deskripsi Mata Kuliah'
            }
            ),
            'smt': forms.RadioSelect(),
            'ruang': forms.TextInput(attrs={
                'class':'form-control',
                'placeholder':'Nomor Ruang'
            }
            ),
        }