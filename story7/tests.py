from django.test import TestCase
from django.urls import resolve, reverse
from django.test import Client
from django.apps import apps
from .views import pamer
from .apps import Story7Config

from django.http import HttpRequest
# Create your tests here.

class UrlsTest(TestCase):
    def setUp(self):
        self.pamer = reverse("pamer")

    def test_pamer_user_right_func(self):
        found = resolve(self.pamer)
        self.assertEqual(found.func, pamer)


class ViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.pamer = reverse("pamer")

    def test_GET_pamer(self):
        response = self.client.get(self.pamer)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'pamer.html')

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(Story7Config.name, 'story7')
        self.assertEqual(apps.get_app_config('Story7').name, 'story7')    
