from . import views
from django.urls import path


urlpatterns = [
    path('', views.mencari, name="mencari"),
    path('library/', views.library, name="library")
]
