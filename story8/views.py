from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
import requests
import json

# Create your views here.


def mencari(request):
    return render(request, 'search.html')


def library(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)
