from django.urls import path, include
from . import views

app_name = 'story9'

urlpatterns = [
    # path('',views.home9,name="home9"),
    path('',include("django.contrib.auth.urls")),
    path('register',views.register9,name='register9'),
    path('logged_in',views.logged_in9,name='logged_in9'),
]
