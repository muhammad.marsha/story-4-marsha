from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
# def home9(request):
#     return render(request,'Story9/homelog.html')

def logged_in9(request):
    if request.user.is_authenticated:
        return render(request,'Story9/logged_in.html', {'nama':request.user.username})
    else:
        return redirect('/story9/login')

def register9(request):
    if request.user.is_authenticated:
        return redirect('/story9/logged_in')
    else:
        if request.method == 'POST':
            forms = UserCreationForm(request.POST)
            if forms.is_valid():
                forms.save()
                return redirect('/story9/login')

        form = UserCreationForm()
        return render(request, 'Story9/register.html', {"form":form})
