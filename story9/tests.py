from django.test import TestCase, Client
from django.urls import resolve

from .apps import Story9Config
from django.contrib.auth.models import User
from .views import logged_in9, register9

# Create your tests here.

class testLogin(TestCase):
    def test_index_url_is_exist(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code,301)

class testLoggedIn(TestCase):
    def test_index_url_is_exist(self):
        c = Client()
        user = User.objects.create(username="test1")
        user.set_password('initestaja123')
        user.save()
        c.login(username="test1", password='initestaja123')
        logged_in_response = c.get('/story9/logged_in')
        response = Client().get('/story9/logged_in')
        self.assertEqual(logged_in_response.status_code,200)
        self.assertEqual(response.status_code,302)
    
    def test_index_func(self):
        c = Client()
        user = User.objects.create(username="test1")
        user.set_password('initestaja123')
        user.save()
        c.login(username="test1", password='initestaja123')
        found = resolve('/story9/logged_in')
        self.assertEqual(found.func,logged_in9)

    def test_index_template(self):
        c = Client()
        user = User.objects.create(username="test1")
        user.set_password('initestaja123')
        user.save()
        c.login(username="test1", password='initestaja123')
        response = c.get('/story9/logged_in')
        self.assertTemplateUsed(response, 'Story9/logged_in.html')


class testRegister(TestCase):
    def test_index_url_is_exist(self):
        c = Client()
        user = User.objects.create(username="test1")
        user.set_password('initestaja123')
        user.save()
        c.login(username="test1", password='initestaja123')
        logged_in_response = c.get('/story9/register')
        response = Client().get('/story9/register')
        self.assertEqual(response.status_code,200)
        self.assertEqual(logged_in_response.status_code,302)
    
    def test_index_func(self):
        found = resolve('/story9/register')
        self.assertEqual(found.func,register9)

    def test_index_template(self):
        response = Client().get('/story9/register')
        self.assertTemplateUsed(response, 'Story9/register.html')
    
    def test_post_method(self):
        response = Client().post('/story9/register',
        data={'username':'test1','password1':'initestaja123','password2':'initestaja123'})
        self.assertEqual(response.status_code,302)

class TestApp(TestCase):
    def test_app_name(self):
        self.assertEqual(Story9Config.name,'story9')
