# Generated by Django 3.1.2 on 2020-10-15 16:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('marsha', '0003_auto_20201015_1431'),
    ]

    operations = [
        migrations.AddField(
            model_name='kelasmodel',
            name='TA',
            field=models.CharField(choices=[('2019/2020', '2019/2020'), ('2020/2021', '2020/2021'), ('2021/2022', '2021/2022'), ('2022/2023', '2022/2023'), ('2023/2024', '2023/2024')], default='2019/2020', max_length=50),
        ),
        migrations.AlterField(
            model_name='kelasmodel',
            name='ruang',
            field=models.IntegerField(),
        ),
        migrations.AlterField(
            model_name='kelasmodel',
            name='smt',
            field=models.CharField(choices=[('Genap', 'Genap'), ('Gasal', 'Gasal')], max_length=10),
        ),
    ]
