from django.urls import path

from . import views

urlpatterns = [
    path('', views.home, name='marsha'),
    path('cs/', views.comingsoon, name = 'comingsoon'),
]