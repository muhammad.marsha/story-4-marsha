from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse
# Create your views here.


def home(request):
    return render(request, 'home.html')

def comingsoon(request):
    return render(request, 'comingsoon.html')