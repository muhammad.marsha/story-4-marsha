from django.db import models

# Create your models here.

class KelasModel(models.Model):
    nama = models.CharField(
        max_length = 30,
        )
    dosen = models.CharField(max_length = 30)
    sks = models.CharField(max_length = 10,
        choices= [
            ('1','1'),
            ('2','2'),
            ('3','3'),
            ('4','4'),
            ('5','5'),
            ('6','6'),   

        ],
    )
    deskripsi = models.CharField(max_length = 100)
    smt = models.CharField(max_length =10,
        choices = [
            ('Gasal','Gasal'),
            ('Genap','Genap'),
        ],
        default = "Gasal"
    )
    TA = models.CharField(
        max_length = 50,
        choices = [
            ('2019/2020','2019/2020'),
            ('2020/2021','2020/2021'),
            ('2021/2022','2021/2022'),
            ('2022/2023','2022/2023'),
            ('2023/2024','2023/2024'),
        ],
    )    

    ruang = models.IntegerField()


    published = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}. {}".format(self.id, self.nama)